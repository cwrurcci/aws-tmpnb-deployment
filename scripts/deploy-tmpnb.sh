#!/bin/bash

# Install epel-release
yum -y install epel-release

# Install docker
cat >/etc/yum.repos.d/docker.repo <<-EOF
[dockerrepo]
name=Docker Repository
baseurl=https://yum.dockerproject.org/repo/main/centos/7
enabled=1
gpgcheck=1
gpgkey=https://yum.dockerproject.org/gpg
EOF

# Install docker-engine
yum -y install docker-engine

# Start docker
service docker start
chkconfig docker on

# Install python34
yum -y install wget unzip bzip2 vim-common

#ec2-metadata
cd /usr/bin
wget http://s3.amazonaws.com/ec2metadata/ec2-metadata
chmod +x /usr/bin/ec2-metadata
hostname=`ec2-metadata --public-hostname | awk '{print $2}'`

echo "registering cwru-tmpnb.duckdns.org"
ec2ip=`ec2-metadata --public-ipv4 | awk '{print $2}'`
curl -k "https://www.duckdns.org/update?domains=cwru-tmpnb&token=a25f0fe3-f90f-49d8-9dd9-77195027d0b0&ip=$ec2ip"

# Install Apache
yum -y install httpd mod_ssl
/usr/sbin/setsebool httpd_can_network_connect true

# download and install mod_auth_cas repo
wget https://bitbucket.org/cwrurcci/aws-tmpnb-deployment/raw/ce15818cea5eff7c226cda49c9cdc5ec10cb63bc/rpms/x86_64/mod_auth_cas-1.1-1.el7.centos.x86_64.rpm

rpm -ivh mod_auth_cas-1.1-1.el7.centos.x86_64.rpm

# Apache CAS/Proxy Config
# You need to set Apache ServerName for CAS to work 
cat >/etc/httpd/conf.d/jupyter.conf <<-EOF
<VirtualHost *:80>

	CASCookiePath /var/cache/httpd/mod_auth_cas/
	CASLoginURL https://login.case.edu/cas/login
	CASValidateURL https://login.case.edu/cas/serviceValidate

        ServerName $hostname
        ProxyPreserveHost On
        ProxyRequests off

	<Location />
		Authtype CAS
		require valid-user
	</Location>
	
	<Location ~ "/(user/[^/]*)/(api/kernels/[^/]+/channels|terminals/websocket)/?">
                ProxyPass ws://127.0.0.1:8000
                ProxyPassReverse ws://127.0.0.1:8000
        </Location>
	
        ProxyPass / http://127.0.0.1:8000/
        ProxyPassReverse / http://127.0.0.1:8000/

	#<Location ~ "/(user/[^/]*)/static/">
	#	ProxyPass http://cdn.jupyter.org/notebook/try-4.0.4/
	#</Location>
	
</VirtualHost>
EOF

# Start Apache
chown apache:apache /var/cache/httpd/mod_auth_cas/
apachectl start
chkconfig httpd on

#Pull Docker Images
docker pull jupyter/configurable-http-proxy
docker pull jupyter/tmpnb
docker pull jupyter/all-spark-notebook

# Docker run script
cat > /home/centos/run-jupyter.sh  <<-EOF
#!/bin/bash
export TOKEN=$( head -c 30 /dev/urandom | xxd -p )

docker run --net=host -d -e CONFIGPROXY_AUTH_TOKEN=$TOKEN --name=proxy jupyter/configurable-http-proxy --default-target http://127.0.0.1:9999

docker run -d \
    --net=host \
    -e CONFIGPROXY_AUTH_TOKEN=$TOKEN \
    -v /var/run/docker.sock:/docker.sock \
    jupyter/tmpnb \
    python orchestrate.py --image='jupyter/all-spark-notebook' \
        --command='start-notebook.sh \
            "--NotebookApp.base_url={base_path} \
            --ip=0.0.0.0 \
            --port={port} \
            --NotebookApp.trust_xheaders=True"'
EOF
chmod +x /home/centos/run-jupyter.sh



